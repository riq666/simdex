# SimDex

Manga reading site created in React using MangaDEX API. I wanted to practice react and decided to build my own manga reading site. You can see manga authors, read chapters and search mangas with tags. 

![figure-home](https://bucket.riqq-cdn.cf/file/riqcdn/simdex-figure1.png) 

![figure-ready-read](https://bucket.riqq-cdn.cf/file/riqcdn/simdex-figure2.png)
 
